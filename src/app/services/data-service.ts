import { InMemoryDbService } from 'angular-in-memory-web-api';
export class InMemoryDataService implements InMemoryDbService {
  createDb() {
    let heroes = [
      { id: 11, name: 'Mr. Nice Guy', bio: 'Really just an asshole not nice at all' },
      { id: 12, name: 'Narco', bio: 'deals in cocaine, special-k and dark chocolate' },
      { id: 13, name: 'Bombastic Bombasto', bio: 'likes bombs but not a terrorist' },
      { id: 14, name: 'Celeritas', bio: 'Loves the celebraties' },
      { id: 15, name: 'Magneta', bio: 'More of a puce colour' },
      { id: 16, name: 'RubberMan', bio: 'Always uses protection'},
      { id: 17, name: 'Dynama', bio: 'Like the magician dynamo but better fed' },
      { id: 18, name: 'Dr IQ', bio: 'Specialising in neurosurgery' },
      { id: 19, name: 'Magma', bio: 'A demi-god, Vulcans lesser know brother' },
      { id: 20, name: 'Tornado', bio: 'Full of hot air' }
    ];
    return {heroes};
  }
}
