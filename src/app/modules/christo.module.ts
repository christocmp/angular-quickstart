import { NgModule } from '@angular/core';
import { AboutUsComponent } from "../components/about/about-us.component";
import { CompanyInfoComponent } from "../components/company-info/company-info.component";
import { BouncingBallsComponent } from "../components/canvas-example/bouncing-balls.component";
import { RabbitmqExampleComponent } from "../components/rabbitmq-example/rabbitmq-example.component";

@NgModule({
  declarations: [
    AboutUsComponent,
    CompanyInfoComponent,
    BouncingBallsComponent,
    RabbitmqExampleComponent
  ],

})

export class ChristoModule {}
