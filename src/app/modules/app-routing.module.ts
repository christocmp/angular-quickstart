import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent }   from '../components/dashboard/dashboard.component';
import { HeroesComponent }      from '../components/hero/heroes.component';
import { HeroDetailComponent }  from '../components/hero-detail/hero-detail.component';
import { AboutUsComponent }     from '../components/about/about-us.component';
import { ServerCommunicationComponent }     from '../components/server-communication/server-communication.component';
import { BouncingBallsComponent } from "../components/canvas-example/bouncing-balls.component";

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard',  component: DashboardComponent },
  { path: 'hero/:id', component: HeroDetailComponent },
  { path: 'heroes',     component: HeroesComponent },
  { path: 'about-us',     component: AboutUsComponent },
  { path: 'server-comms', component: ServerCommunicationComponent},
  { path: 'canvas', component: BouncingBallsComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})

export class AppRoutingModule {}
