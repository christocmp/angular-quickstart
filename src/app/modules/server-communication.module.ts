import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule }   from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';

import { ServerCommunicationComponent } from '../components/server-communication/server-communication.component';
import { HeroHttpListComponent } from '../components/server-communication/hero-list/hero-list.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    JsonpModule
  ],
  declarations: [ 
      ServerCommunicationComponent,
      HeroHttpListComponent 
  ]
})
export class ServerCommunicationModule {
}