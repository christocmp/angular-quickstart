import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule }    from '@angular/http';
import { AppRoutingModule } from './modules/app-routing.module';
import { ChristoModule } from './modules/christo.module';
// Examples based on official docs section Server Communication
import { ServerCommunicationModule } from './modules/server-communication.module';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/data-service';

import { AppComponent }        from './app.component';
import { HeroDetailComponent } from './components/hero-detail/hero-detail.component';
import { HeroesComponent }     from './components/hero/heroes.component';
import { DashboardComponent }  from './components/dashboard/dashboard.component';
import { HeroService }         from './services/hero.service';
import { HeroSearchComponent } from './components/hero-search/hero-search.component';
import { BouncingBallsComponent } from './components/canvas-example/bouncing-balls.component';


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    AppRoutingModule,
    ChristoModule,
    ServerCommunicationModule
  ],
  declarations: [
    AppComponent,
    HeroDetailComponent,
    HeroesComponent,
    DashboardComponent,
    HeroSearchComponent
  ],
  providers: [ HeroService ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
