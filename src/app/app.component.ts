import { Component } from '@angular/core';
@Component({
  selector: 'my-app',
  template: `
    <h1>{{title}}</h1>
    <h2>What's it all about</h2>
    An expanded form of the angular tutorial to play with new ideas and practice angular
    <nav>
      <a routerLink="/dashboard"  routerLinkActive="active">Dashboard</a>
      <a routerLink="/heroes"  routerLinkActive="active">Heroes</a>
      <a [routerLink]="['/hero', '11']"  routerLinkActive="active">Mr Nice Guy</a>
      <a [routerLink]="['/about-us']"  routerLinkActive="active">About Us</a>
      <a [routerLink]="['/server-comms']"  routerLinkActive="active">Server Comms</a>
      <a [routerLink]="['/canvas']"  routerLinkActive="active">Canvas</a>
    </nav>
    <router-outlet></router-outlet>
  `,
  styleUrls: [ './app.component.css' ],
})
export class AppComponent {
  title = 'Christo\'s Angular playground';
}
