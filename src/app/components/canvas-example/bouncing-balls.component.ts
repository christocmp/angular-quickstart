import { Component, Input, AfterViewInit } from '@angular/core';
import { Circle } from '../../classes/circle';
import {Observable} from 'rxjs/Rx';

@Component({
    selector: 'bouncing-balls',
    templateUrl: './bouncing-balls.component.html',
    styleUrls: ['./bouncing-balls.component.css']
})

export class BouncingBallsComponent implements AfterViewInit {
    private c: CanvasRenderingContext2D;
    private canvas: HTMLCanvasElement;
    private circleArray: Array<Circle> = [];
    private animationId: any;

    private circleSource$ = Observable.from(this.circleArray);
    private moveStream$ =  Observable.fromEvent(document, 'mousemove');

    ngAfterViewInit() {
        console.log('bouncing ball canvas has not been initialised');
        this.canvas = document.querySelector('canvas');
        this.canvas.width = 800; //window.innerWidth;
        this.canvas.height = 400; //window.innerHeight;
        this.c = this.canvas.getContext("2d");

        let i = 0;
        for (i = 0; i <= 10; i++) {
            let x = (Math.random() * 200) + 30;
            let y = Math.random() * 200 + 30;
            let radius = Math.random() * 30;
            let dx = Math.random() * 20;
            let dy = Math.random() * 20;
            let tempColour = (Math.random() * 100) + 30;
            let newCircle = new Circle(x, y, radius, dx, dy, "#ff2211");
            this.circleArray.push(newCircle);
        }
        this.circleSource$.subscribe(x => {console.log('y =', x.y, 'x =', x.x)});


        this.animate();
    }

    start(){
        console.log('bouncing balls started');
        if (!this.animationId) {
            this.animate();
        }
    }

    add(n: number){
      let i = 0;
      for (i = 0; i <= n; i++) {
        let x = (Math.random() * 200) + 30;
        let y = Math.random() * 200 + 30;
        let radius = Math.random() * 30;
        let dx = Math.random() * 20;
        let dy = Math.random() * 20;
        let newCircle = new Circle(x, y, radius, dx, dy, "#393bff");
        this.circleArray.push(newCircle);
      }
    }

    canvasClick(){
      console.log('canvas click');
    }

    stop(){
      console.log('bouncing balls stopped');
      console.dir(this.animationId);
        if (this.animationId) {
            cancelAnimationFrame(this.animationId);
            this.animationId = undefined;
        }
    }

    heat(){
      console.log('heating up');
      console.dir(this.animationId);
      if (this.animationId) {
        cancelAnimationFrame(this.animationId);
        this.animationId = undefined;
      }
    }

    cool(){
      console.log('cooling down');

      let i = 0;
      for (i = 0; i <= this.circleArray.length; i++) {
        this.circleArray[i].velocity = 100;
        this.circleArray[i].velocity = 100;
      }
    }

    animate() {
        this.animationId = requestAnimationFrame(() => {
            this.animate();
        });
        this.c.clearRect(0, 0, this.canvas.width, this.canvas.height);
        let i = 0;

        for (i = 0; i < this.circleArray.length; i++) {
          /*
          this.circleArray[i].dx = this.circleArray[i].dx - this.circleArray[i].velocity;
          this.circleArray[i].dy = this.circleArray[i].dy - this.circleArray[i].velocity;*/
            this.c.beginPath();
            this.c.arc(this.circleArray[i].x, this.circleArray[i].y, this.circleArray[i].radius, 0, Math.PI * 2, false);
            this.c.strokeStyle = this.circleArray[i].colour;
            if (this.circleArray[i].x > this.canvas.width - this.circleArray[i].radius) {
                this.circleArray[i].dx = -this.circleArray[i].dx;
            }
            if (this.circleArray[i].x <= this.circleArray[i].radius) {
                this.circleArray[i].dx = -this.circleArray[i].dx;
            }
            if (this.circleArray[i].y > this.canvas.height - this.circleArray[i].radius) {
                this.circleArray[i].dy = -this.circleArray[i].dy;
            }
            if (this.circleArray[i].y <= this.circleArray[i].radius) {
                this.circleArray[i].dy = -this.circleArray[i].dy;
            }
            this.circleArray[i].x += this.circleArray[i].dx;
            this.circleArray[i].y += this.circleArray[i].dy;
            this.c.stroke();

        }
    }

}
