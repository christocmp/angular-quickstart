import { Component, Input } from '@angular/core';


@Component({
  selector: 'company-info',
  templateUrl: './company-info.component.html',
  styleUrls: [ './company-info.component.css' ]
})
export class CompanyInfoComponent {
  @Input() timeSinceLastIncident: string;

}
