// Observable Version
import { Component, OnInit } from '@angular/core';
import { Hero }              from '../../../classes/hero';
import { HeroHttpService }       from '../../../services/hero-http.service';

@Component({
  selector: 'hero-list-http',
  templateUrl: './hero-list.component.html',
  providers: [ HeroHttpService ],
  styles: ['.error {color:red;}']
})

export class HeroHttpListComponent implements OnInit {
    errorMessage: string;
    heroes: Hero[];
    mode = 'Observable';

    constructor(private heroHttpService: HeroHttpService) { }

    ngOnInit() { this.getHeroes(); }

    getHeroes() {
        this.heroHttpService.getHeroes()
            .subscribe(
                heroes => this.heroes = heroes,
                error => this.errorMessage = <any>error
            );
    }
/*
    addHero(name: string) {
        if (!name) { return; }
        this.heroHttpService.create(name)
            .subscribe(
            hero => this.heroes.push(hero),
            error => this.errorMessage = <any>error);
    }*/
}