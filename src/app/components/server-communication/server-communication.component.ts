import { Component } from '@angular/core';

@Component({
    selector: 'my-app',
    template: `<h1>server communication placeholder</h1>
    <hero-list-http></hero-list-http>`
    /*
    template: `
    <hero-list></hero-list>
    <hero-list-promise></hero-list-promise>
    <my-wiki></my-wiki>
    <my-wiki-smart></my-wiki-smart>
  `*/
})
export class ServerCommunicationComponent { }