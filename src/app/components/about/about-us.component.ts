import { Component, Input } from '@angular/core';


@Component({
  selector: 'about-us',
  templateUrl: './about-us.component.html',
  styleUrls: [ './about-us.component.css' ]
})
export class AboutUsComponent {
  @Input() testForPropertyBinding: string;

  private valueBindingText = 'Value binding example';
  private propertyBindingText = 'Property binding example';
  private modelBindingText = 'Model binding example';

  onMoreInfo() {
    console.log('more info has been clicked in about us');
  }


}
