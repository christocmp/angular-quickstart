import {Component, Input, Output, EventEmitter, OnInit} from '@angular/core';
import { ActivatedRoute, Params }   from '@angular/router';
import { Location }                 from '@angular/common';
import { HeroService } from '../../services/hero.service';
import { Hero } from '../../classes/hero';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'hero-detail',
  templateUrl: './hero-detail.component.html'
})
export class HeroDetailComponent implements OnInit{
  @Input() hero: Hero;
  @Output() randomHeroAdded = new EventEmitter<boolean>();

  constructor(
    private heroService: HeroService,
    private route: ActivatedRoute,
    private location: Location
  ) {}

  ngOnInit(): void {
    //if noroute parameter e.g coming from heroes.component not url (data loaded todo find a better way)
    //if (this.route.params.value.id !== NaN && this.route.params.value.id !== undefined) {
        this.route.params
        .switchMap((params: Params) => this.heroService.getHero(+params['id']))
        .subscribe(hero => this.hero = hero);
    //}
  }

  onAddRandomHero(): void {
    console.log('emitting add random hero');
    this.randomHeroAdded.emit();
  }

  goBack(): void {
    this.location.back();
  }

  save(): void {
    this.heroService.update(this.hero)
    //  .then(() => this.goBack());
  }
}
