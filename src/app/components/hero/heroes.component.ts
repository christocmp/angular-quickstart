import { Component, OnInit } from '@angular/core';
import { Hero } from '../../classes/hero';
import { HeroService } from '../../services/hero.service';
import { Router } from '@angular/router';

@Component({
  selector: 'my-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: [ './heroes.component.css' ],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit{

  heroes: Hero[];

  private selectedHero: Hero;
  private lastId = 20;
  //selectedHero = this.heroes[0]; // none selected by default

  constructor(
    private router: Router,
    private heroService: HeroService
  ){ }

  ngOnInit(): void {
    this.getHeroes();
  }

  getHeroes(): void {
    this.heroService.getHeroes().then(result => {this.heroes = result, this.sortAlphabetically();}, () => {console.log('there has been an error');})
  }

/* here is the non arrow function way
  getHeroes(): void {
    var _self = this;
    this.heroService.getHeroes().then(function(result) {
     _self.heroes = result; },
     function(error){
      console.log('there has been an error')
     });
  }
*/
  sortAlphabetically(){
    this.heroes.sort(function(a, b) {
      let nameA = a.name.toUpperCase(); // ignore upper and lowercase
      let nameB = b.name.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      // names must be equal
      return 0;
    });
  }


  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  onBlur(): void {
    this.sortAlphabetically();
  }

  randomHeroAdded(){
    let heroName = this.generateRandomName();

    this.heroService.create(heroName)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = hero;
        //this.sortAlphabetically();
      });
  }

  generateRandomName()
  {
    let firstName = "";
    let lastName = "";
    let possibleCharacters = "abcdefghijklmnopqrstuvwxyz";

    for( var i=0; i < 6; i++ ) {
      firstName += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
    }

    for( var i=0; i < 9; i++ ) {
      lastName += possibleCharacters.charAt(Math.floor(Math.random() * possibleCharacters.length));
    }

    return firstName +' '+ lastName;
  }

  gotoDetail(): void {
    this.router.navigate(['/hero', this.selectedHero.id]);
  }

  add(name: string): void {
    name = name.trim();
    if (!name) { return; }
    this.heroService.create(name)
      .then(hero => {
        this.heroes.push(hero);
        this.selectedHero = null;
        //this.sortAlphabetically();
      });
  }

  delete(hero: Hero): void {
    this.heroService
      .delete(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero === hero) { this.selectedHero = null; }
      });
  }
}
